import extractTracked from "../../src/domain/extractTracked";
import {validate} from "../../src/domain/validate";
import {InvalidMixpanelEventNameError} from "../../src/errors/InvalidMixpanelEventNameError";
import MissingOrInvalidMixpanelPropError from "../../src/errors/MissingOrInvalidMixpanelPropError";

const btoa = source => Buffer.from(source).toString('base64');

describe('When passed a MixpanelTracking object', () => {
    const mixpanel = {
        event: 'some name',
        properties: {
            prop1: 'value1',
            prop2: 42,
        }
    };

    it('returns an InvalidMixpanelEventNameError if tracked wrong event name', () => {
        expect(validate('different name', {}, {}, mixpanel))
            .toBeInstanceOf(InvalidMixpanelEventNameError);
    });

    it('returns an array of MissingOrInvalidMixpanelPropError if event name is correct but a prop is missing', () => {
        expect(validate('some name', { prop3: 'v3' }, {}, mixpanel)[0])
            .toBeInstanceOf(MissingOrInvalidMixpanelPropError);
    });

    it('returns an array of MissingOrInvalidMixpanelPropError if event name is correct but a prop tracked with wrong value', () => {
        expect(validate('some name', { prop2: 'v3' }, {}, mixpanel)[0])
            .toBeInstanceOf(MissingOrInvalidMixpanelPropError);
    });

    it('returns true if event name is correct and all required props have good value', () => {
        expect(validate('some name', { prop1: 'value1' }, {}, mixpanel))
            .toEqual(true);
        expect(validate('some name', { prop2: 42 }, {}, mixpanel))
            .toEqual(true);
        expect(validate('some name', { prop1: 'value1', prop2: 42 }, {}, mixpanel))
            .toEqual(true);
    });

    it('returns an array of MissingOrInvalidMixpanelPropError if some required props have good value but some are missing or invalid', () => {
        expect(validate('some name', { prop1: 'value1', prop3: 3 }, {}, mixpanel)[0])
            .toBeInstanceOf(MissingOrInvalidMixpanelPropError);
        expect(validate('some name', { prop2: 42, prop4: 'p4' }, {}, mixpanel)[0])
            .toBeInstanceOf(MissingOrInvalidMixpanelPropError);
        expect(validate('some name', { prop1: 'value1', prop2: 42, anotherOne: 'one' }, {}, mixpanel)[0])
            .toBeInstanceOf(MissingOrInvalidMixpanelPropError);
    });
});
