import extractTracked from "../../src/domain/extractTracked";

const btoa = source => Buffer.from(source).toString('base64');

describe('When passed a ListenedXHR object', () => {
    it('returns the tracked object', () => {
        const xhr = {
            status: 'success',
            url: `http://api.mixpanel.com/track/?data=${btoa(JSON.stringify({
                event: 'some event',
                properties: {}
            }))}`
        };

        expect(extractTracked({}, xhr)).toEqual({
            event: 'some event',
            properties: {},
        });
    });

    it('works with HTTPS url', () => {
        const xhr = {
            status: 'success',
            url: `https://api.mixpanel.com/track/?data=${btoa(JSON.stringify({
                event: 'some event',
                properties: {}
            }))}`
        };

        expect(extractTracked({}, xhr)).toEqual({
            event: 'some event',
            properties: {},
        });
    });

    it('works with getMixpanelEventFromUrl', () => {
        const getMixpanelEventFromUrl = url => url.split('?data=')[1]

        const xhr = {
            status: 'success',
            url: `https://custom.endpoint.com/track/?data=${btoa(JSON.stringify({
                event: 'some event',
                properties: {}
            }))}`
        };

        expect(extractTracked({ getMixpanelEventFromUrl }, xhr)).toEqual({
            event: 'some event',
            properties: {},
        });
    });

    it('throw an error with empty data', () => {
      try {
        extractTracked({}, { status: 'success', url: `https://api.mixpanel.com/track/?data=` });
      } catch (e) {
        expect(e).toEqual({
          error: 'Could not find any Mixpanel data in this request',
        });
      }
    });
});
