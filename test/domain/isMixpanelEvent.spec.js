import isMixpanelEvent from "../../src/domain/isMixpanelEvent";

const btoa = source => Buffer.from(source).toString('base64');

describe('When passed a Mixpanel tracking object', () => {
    const mixpanel = {
        event: 'some name',
        properties: {}
    };

    it('returns false if event name is different', () => {
        expect(isMixpanelEvent('other name', {}, mixpanel)).toEqual(false);
    });

    it('returns true if event name with tested name', () => {
        expect(isMixpanelEvent('some name', {}, mixpanel)).toEqual(true);
    });
});

describe('When passed a ListenedXHR object', () => {
    const mixpanel = {
        status: 'success',
        url: `http://api.mixpanel.com/track/?data=${btoa(JSON.stringify({
            event: 'some event',
            properties: {}
        }))}`
    };

    it('returns false if event name is different', () => {
        expect(isMixpanelEvent('other name', {}, mixpanel)).toEqual(false);
    });

    it('returns true if event name with tested name', () => {
        expect(isMixpanelEvent('some event', {}, mixpanel)).toEqual(true);
    });

    it('returns false if event name with tested name but wrong host regExp', () => {
        const hostRegex = /https?:\/\/custom.endpoint.com\/track\/\?data=([^&]+).*/;
        expect(isMixpanelEvent('other name', { hostRegex }, mixpanel)).toEqual(false);
    });
});
