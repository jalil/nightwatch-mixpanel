// @flow
import type { MixpanelTracking } from "../types.flow";

export default class MissingOrInvalidMixpanelPropError extends Error {
    constructor(propKey: string, propValue:(string | number), listened:MixpanelTracking) {
        super('\n'
            + `Invalid Mixpanel value for property '${propKey}'\n`
            + `Expected '${propValue}'\n`
            + `Got '${listened.properties[propKey]}'\n\n`);
        this.name = 'Missing or Invalid Mixpanel property';
    }
}
