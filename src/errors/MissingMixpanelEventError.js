// @flow
import type { MixpanelTracking } from "../types.flow";

export default class MissingMixpanelEventError extends Error {
    constructor(eventName:string) {
        super('\n'
            + `Missing Mixpanel event '${eventName}'\n`);
        this.name = `Missing Mixpanel event '${eventName}'\n`;
    }
}
