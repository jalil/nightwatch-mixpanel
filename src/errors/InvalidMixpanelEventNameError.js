// @flow

import type {MixpanelTracking} from "../types.flow";

export class InvalidMixpanelEventNameError extends Error {
    constructor(expectedEvent:string, listened:MixpanelTracking) {
        super('\n'
            + 'Invalid Mixpanel event name\n'
            + `Expected '${expectedEvent}'\n`
            + `Got '${listened.event}'\n\n`);
        this.name = 'Invalid Mixpanel event name';
    }
}