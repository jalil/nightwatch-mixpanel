// @flow

type URL = string;
export opaque type UUID = string;
export opaque type milliseconds = number;
export opaque type HTTPMethod = 'GET' | 'POST' | 'PUT' | 'HEAD' | 'CONNECT' | 'PATCH' | 'DELETE' | 'OPTIONS' | 'TRACE';
export opaque type XHRStatus = 'success' | 'error';

export type ListenedXHR = {|
    id: UUID,
    openedTime: milliseconds,
    method: HTTPMethod,
    url: URL,
    status: XHRStatus,
    httpResponseCode?: number,
    responseData?: string,
    requestData?: string,
|};

export type MixpanelTracking = {| event:string, properties: {} |};

export type Options = { hostRegex?:RegExp, getMixpanelEventFromUrl?: URL => ?string }

export type Callback = Array<ListenedXHR> => void;

export type Trigger = void => void;
