// @flow
import type { Callback, ListenedXHR, Trigger } from "../types.flow";

const util = require('util');
const events = require('events');

function WaitForMixpanel() {
    //$FlowFixMe
    events.EventEmitter.call(this);
}

util.inherits(WaitForMixpanel, events.EventEmitter);

WaitForMixpanel.prototype.command = function (
    delay: number = 1000,
    trigger: Trigger = () => {},
    expectedEvents: Array<[string, {}]>,
    callback:Callback,
) {
    const command = this;
    setImmediate(
        () => command.api.waitForXHR(
            'mixpanel.com',
            delay,
            trigger,
            xhrs => {
                expectedEvents.forEach(
                    ([mpEvent, mpProps]) => {command.api.assert.mixpanelTracked(mpEvent, mpProps, xhrs)}
                );
                if (callback)
                    callback(xhrs);
                command.emit('complete');
            }
        )
    );

    return this;
};

module.exports = WaitForMixpanel;
