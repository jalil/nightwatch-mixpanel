// @flow
import R from 'ramda';
import type { Options, ListenedXHR, MixpanelTracking } from '../types.flow';

type ExtractTracked = (?Options, ListenedXHR) => MixpanelTracking;

const DEFAULT_REGEX:RegExp = /https?:\/\/api.mixpanel.com\/track\/\?data=([^&]+).*/;

const atob = (b64Encoded:string):string => Buffer.from(b64Encoded, 'base64').toString();

type DefaultGetMixpanelFromUrl =  string => ?string
const defaultGetMixpanelFromUrl:DefaultGetMixpanelFromUrl = R.pipe(
  R.match(DEFAULT_REGEX),
  R.nth(1)
)

const extractTracked:ExtractTracked = (options, listenedXHR) => R.pipe(
    R.prop('url'),
    (options && options.getMixpanelEventFromUrl) || defaultGetMixpanelFromUrl,
    R.ifElse(
      R.both(R.complement(R.isNil), R.complement(R.isEmpty)),
      R.pipe(
        decodeURIComponent,
        atob,
        JSON.parse
      ),
      R.always(new Error('Could not find any Mixpanel data in this request'))
    )
)(listenedXHR);

export default R.curry(extractTracked);
