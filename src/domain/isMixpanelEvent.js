// @flow
import R from 'ramda';
import type { ListenedXHR, MixpanelTracking, Options } from '../types.flow';
import extractTracked from './extractTracked';

const isXHREvent = R.has('url');
const hasEventName = R.propEq('event');

const isMixpanelEvent = R.curry(
        (eventName:string, options:Options, mixpanelEvent:ListenedXHR | MixpanelTracking) =>
        R.compose(
            hasEventName(eventName),
            R.when(isXHREvent, extractTracked(options))
        )(mixpanelEvent)
    );

export default isMixpanelEvent;
