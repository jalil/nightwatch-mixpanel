// @flow
import R from 'ramda';
import type { MixpanelTracking, Options } from "../types.flow";
import isMixpanelEvent from "./isMixpanelEvent";
import { InvalidMixpanelEventNameError } from "../errors/InvalidMixpanelEventNameError";
import MissingOrInvalidMixpanelPropError from "../errors/MissingOrInvalidMixpanelPropError";

const clean = (value:mixed):mixed => {
    if (typeof value === 'string')
        return value.trim();

    return value;
};

const missingOrInvalidProp = (listened:MixpanelTracking) =>
    ([propKey, propValue]) =>
        listened && clean(listened.properties[propKey]) !== clean(propValue);

export const validate = R.curry(
    (expectedEvent:string, expectedProps:{}, options:Options, listened:MixpanelTracking):(Error|Array<boolean|Error>) => {
        if (!isMixpanelEvent(expectedEvent, options, listened))
            return new InvalidMixpanelEventNameError(expectedEvent, listened);

        return R.compose(
            R.ifElse(
                R.isEmpty,
                R.always(true),
                R.map(
                    ([propKey, propValue]) => new MissingOrInvalidMixpanelPropError(propKey, propValue, listened),
                )
            ),
            R.filter(missingOrInvalidProp(listened)),
            R.toPairs,
        )(expectedProps);
    }
);
