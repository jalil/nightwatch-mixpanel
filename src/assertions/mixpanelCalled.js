// @flow
import R from 'ramda';
import type { ListenedXHR, XHRStatus } from '../types.flow';

exports.assertion = function mixpanelCalled(listenedXhrs:Array<ListenedXHR>, event:RegExp = /.*/) {
    this.message = `Mixpanel event '${event.toString()}' called`;
    this.expected = 'success';
    this.pass = R.allPass([
        R.is(Array),
        R.compose(
            R.complement(R.isEmpty),
            R.filter(R.compose(
                R.match(event), R.prop('url')
            )),
        ),
    ]);
    this.value = result => result;
    this.command = function command(callback:(Array<ListenedXHR> => void)) {
        callback(listenedXhrs);
        return this;
    };
};
