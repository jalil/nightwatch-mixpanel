// @flow
import R from 'ramda';
import type { ListenedXHR, MixpanelTracking, Options } from '../types.flow';
import extractTracked from '../domain/extractTracked';
import { validate } from "../domain/validate";
import MissingMixpanelEventError from "../errors/MissingMixpanelEventError";

const logErrors = (errors:Array<Error>):Array<Error> => {
    // eslint-disable-next-line no-console
    console.log('Expected Mixpanel event not found. Got : ');
    errors.forEach(
        error => {
            // eslint-disable-next-line no-console
            console.error(error.message);
        }
    );
    return errors;
};

exports.assertion = function (mpEvent:string, mpProps:{}, options:?Options = {}, listenedXhrs:ListenedXHR|Array<ListenedXHR>) {
    this.message = `Expected Mixpanel '${mpEvent}' tracked ${R.isEmpty(mpProps) ? '' : 'with props'}`;
    this.expected = mpProps;
    this.pass = function pass(xhrs:Array<MixpanelTracking>):boolean {
        if (!xhrs.length) {
            logErrors([new MissingMixpanelEventError(mpEvent)]);
            return false;
        }

        const validations = xhrs.map((xhr:MixpanelTracking):(boolean|Error) => validate(mpEvent, mpProps, options, xhr));

        if (validations.find(v => v === true))
            return true;

        // $FlowFixMe
        R.compose(
            logErrors,
            R.flatten,
            R.filter(R.complement(R.is(Error))),
        )(validations);
        return false;
    };
    this.value = function value(result) { return result; };
    this.command = function command(callback) {
        const self = this;
        const xhrs:Array<ListenedXHR> = Array.isArray(listenedXhrs) ? listenedXhrs : [listenedXhrs];

        if (listenedXhrs) {
            setImmediate(() => {
                callback.call(self, xhrs.map(extractTracked(options)));
            });
        } else {
            setImmediate(() => {
                callback.call(self, []);
            });
        }
        return this;
    };
};
