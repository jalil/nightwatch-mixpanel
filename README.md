# nightwatch-mixpanel
[![All Contributors](https://img.shields.io/badge/all_contributors-2-orange.svg?style=flat-square)](#contributors)

Mixpanel Custom command and assertions for Nightwatch.

This package use nightwatch-xhr, which must be installed. And of course, you must use Nightwatch.js.

## Setup
```
    npm install -D nightwatch-xhr nightwatch-mixpanel
```

or
```
    yarn add -D nightwatch-xhr nightwatch-mixpanel
```

Then, in order for your project to be able to access these commands and assertions you need to include them in your projects Nightwatch config :
```
...
"custom_commands_path": ["./node_modules/nightwatch-xhr/src/commands", "./node_modules/nightwatch-mixpanel/src/commands"],
"custom_assertions_path: ["./node_modules/nightwatch-xhr/src/assertions", "./node_modules/nightwatch-mixpanel/src/assertions"],
...
```

or, for legacy javascript, use

```
...
"custom_commands_path": ["./node_modules/nightwatch-xhr/es5/commands", "./node_modules/nightwatch-mixpanel/es5/commands"],
"custom_assertions_path: ["./node_modules/nightwatch-xhr/es5/assertions", "./node_modules/nightwatch-mixpanel/es5/assertions"],
...
```

## Usage

### Custom Command : `waitForMixpanel`

#### API

The function expects these parameters:
* **delay** - how much time should we listen for Mixpanel requests
* **trigger** - activate a trigger in the browser after initiating the listener
* **expectedMixpanel** - an array of event names and event properties we expect  
* **callback(optional)** - use this to assert the request after it completes

#### Example

Without callback
```javascript
module.exports = {
    'Login Event': function (browser) {
        browser
            .waitForMixpanel(
                1000,
                browser.login(),
                [['Login', { from: 'Homepage' }]],
            )
    }
};
```

With callback:
```javascript
module.exports = {
    'Login Event': function (browser) {
        browser
            .waitForMixpanel(
                1000,
                browser.login(),
                [['Login', { from: 'Homepage' }]],
                xhrs => {
                    browser.assert.equal(xhr[0].httpResponseCode, 200);
                }
            )
    }
};
```

The callback function returns an array of object containing the following properties :
* status (success/error/timeout)
* method (GET/POST)
* url (url of request)
* requestData (raw POSTed data)
* httpResponseCode (HTTP status response code in string, eg: "200", )
* responseData (raw response data)

When the anticipated XHR request has not occurred, it fails an assertion. Callback is not called.

### Custom Assertion : `mixpanelTracked`

You can use directly the custom `mixpanelTracked` assertion on xhr objects returns by nightwatch-xhr's `waitForXHR` custom command.

#### API

The assertion expect :
* **mpEvent** - name of the expected Mixpanel event
* **mpProps** - expected properties of the Mixpanel event
**options** (optional, default to `{}`) - An option object with these properties avalaible :
  * **getMixpanelEventFromUrl** - A function which take a tracking URL as parameter and should return the data at the included in (example: 'https://api.mixpanel.com/track/?data=myData' => 'myData'). This is usefull if you specified `api_host` option to `mixpanel-browser`.
* **listenedXhrs** - Either a single nightwatch-xhr's xhr object, or an array of such objects.

#### Example

Without callback, without options
```javascript
module.exports = {
    'Some Test': function (browser) {
        browser.waitForXHR(
            'mixpanel',
            1000,
            () => browser.url('some/page'),
            xhrs => {
                browser.assert.mixpanelTracked('View Page', { aProp: 'prop' }, {}, xhrs);
                browser.assert.mixpanelTracked('Other Event', { someProp: 42 }, {}, xhrs);
            }
        );
    }
};
```

## Contributors

<!-- ALL-CONTRIBUTORS-LIST:START - Do not remove or modify this section -->
<!-- prettier-ignore -->
| [<img src="https://framagit.org/uploads/-/system/user/avatar/8497/avatar.png" width="100px;"/><br /><sub><b>jalil</b></sub>](https://framagit.org/jalil/)<br />[💻](https://github.com/jalil/nightwatch-mixpanel/commits?author=jalil "Code") [📖](https://github.com/jalil/nightwatch-mixpanel/commits?author=jalil "Documentation") [💡](#example-jalil "Examples") [⚠️](https://github.com/jalil/nightwatch-mixpanel/commits?author=jalil "Tests") [🔧](#tool-jalil "Tools") [👀](#review-jalil "Reviewed Pull Requests") | [<img src="https://avatars3.githubusercontent.com/u/8048092?v=4" width="100px;"/><br /><sub><b>rdbmax</b></sub>](https://github.com/rdbmax)<br />[💻](https://github.com/jalil/nightwatch-mixpanel/commits?author=rdbmax "Code") [⚠️](https://github.com/jalil/nightwatch-mixpanel/commits?author=rdbmax "Tests") |
| :---: | :---: |
<!-- ALL-CONTRIBUTORS-LIST:END -->

## Contribute

This project follows the all-contributors specification. Contributions of any kind are welcome!
